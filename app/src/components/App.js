import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import ArticleList from './ArticleList';
import articles from '../fixtures';

class App extends Component {
  render() {
    return (
      <div className="container">
         <div className="jumbotron">
            <h1 className="display-3"> App Name</h1>
         </div>
            <div>
               <ArticleList articles={articles} />
            </div>
      </div>
    );
  }
}

export default App;
